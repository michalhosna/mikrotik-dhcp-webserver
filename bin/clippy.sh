#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

set -x
# project root
cd "$(dirname "$DIR")"

cargo clippy --all-targets --all-features -- \
  -D warnings \
  -A clippy::manual_non_exhaustive \
  -A clippy::needless_return

cargo clippy --all-targets -- \
  -D warnings \
  -A clippy::manual_non_exhaustive \
  -A clippy::needless_return


# Not using manual_non_exhaustive because #[non_exhaustive] is always crate scoped
# Private field can be scoped manually
#
# See https://doc.rust-lang.org/reference/visibility-and-privacy.html
#
# > Outside of the defining crate, types annotated with non_exhaustive have limitations
# > that preserve backwards compatibility when new fields or variants are added.
# > https://doc.rust-lang.org/reference/attributes/type_system.html#the-non_exhaustive-attribute
