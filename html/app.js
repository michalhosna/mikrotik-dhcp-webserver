// Inspired by on: https://github.com/alitursucular/filter-and-sort-dynamically-created-table-with-vanilla-javascript-demo/blob/master/js/index.js


const tbody = document.querySelector('tbody');
const thead = document.querySelector('thead');
const th = document.querySelectorAll('thead th');
const err = document.querySelector('.err');

let filterInput = document.querySelector('.filter-input');
const entries = document.querySelectorAll('.entry');

filterInput.addEventListener('keyup', () => {
    let query = filterInput.value.toUpperCase().trim();
    let j = 0;

    entries.forEach((data) => {
        thead.style.opacity = '1'
        err.style.display = '';
        if (data.innerText.toUpperCase().indexOf(query) > -1) {
            data.style.display = '';
        } else {
            data.style.display = 'none';
            j++;
            if (j === entries.length) {
                thead.style.opacity = '0.2'
                err.style.display = 'flex';
            }
        }
    });
});

let sortDirection;

th.forEach((columnHeader, columnI) => {
    columnHeader.addEventListener('click', () => {
        sortDirection = !sortDirection;

        tbody.replaceChildren(...Array.from(entries).sort((a, b) => {
            let lhs = a.querySelectorAll("td")[columnI].innerHTML;
            let rhs = b.querySelectorAll("td")[columnI].innerHTML;


            if (columnI === 0) {
                lhs = lhs.split('.')
                    .map(p => parseInt(p))
                    .reverse()
                    .reduce((acc,val,i) => acc+(val*(256**i)),0)

                rhs = rhs.split('.')
                    .map(p => parseInt(p))
                    .reverse()
                    .reduce((acc,val,i) => acc+(val*(256**i)),0)
            }

            return sortDirection ? lhs > rhs : lhs < rhs;
        }));
    });
});

