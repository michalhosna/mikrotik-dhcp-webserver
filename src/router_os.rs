use std::collections::BTreeMap;

use async_std::io::prelude::*;
use serde::{Deserialize, Deserializer};

pub struct ApiRos<'a, T: Read + Write + Unpin> {
    stream: &'a mut T,
}

#[allow(dead_code)]
impl<'a, T: Read + Write + Unpin> ApiRos<'a, T> {
    pub fn new(s: &'a mut T) -> ApiRos<'a, T> {
        ApiRos { stream: s }
    }

    pub async fn try_read(&mut self) -> bool {
        self.stream
            .read(&mut [0])
            .await
            .map(|i| i > 0)
            .unwrap_or(false)
    }

    async fn write_str(&mut self, str_buff: &[u8]) {
        match self.stream.write(str_buff).await {
            Ok(_) => {},
            Err(e) => {
                panic!("connection closed by remote end, {}", e);
            },
        }
    }

    async fn read_str(&mut self, length: usize) -> String {
        let mut buff: Vec<u8> = Vec::new();
        for _ in 0..length {
            let mut tmp_buff: [u8; 1] = [0];
            match self.stream.read(&mut tmp_buff).await {
                Ok(_) => {},
                Err(e) => {
                    panic!("connection closed by remote end, {}", e);
                },
            }
            buff.push(tmp_buff[0]);
        }
        String::from_utf8(buff).unwrap()
    }

    async fn write_len(&mut self, len: u32) {
        if len < 0x80 {
            self.write_str(&[len as u8]).await;
        } else if len < 0x4000 {
            let l = len | 0x8000;
            self.write_str(&[((l >> 8) & 0xFF) as u8]).await;
            self.write_str(&[(l & 0xFF) as u8]).await;
        } else if len < 0x200000 {
            let l = len | 0xC00000;
            self.write_str(&[((l >> 16) & 0xFF) as u8]).await;
            self.write_str(&[((l >> 8) & 0xFF) as u8]).await;
            self.write_str(&[(l & 0xFF) as u8]).await;
        } else if len < 0x10000000 {
            let l = len | 0xE0000000;
            self.write_str(&[((l >> 16) & 0xFF) as u8]).await;
            self.write_str(&[((l >> 24) & 0xFF) as u8]).await;
            self.write_str(&[((l >> 8) & 0xFF) as u8]).await;
            self.write_str(&[(l & 0xFF) as u8]).await;
        } else {
            self.write_str(&[0xF0_u8]).await;
            self.write_str(&[((len >> 24) & 0xFF) as u8]).await;
            self.write_str(&[((len >> 16) & 0xFF) as u8]).await;
            self.write_str(&[((len >> 8) & 0xFF) as u8]).await;
            self.write_str(&[(len & 0xFF) as u8]).await;
        }
    }

    async fn read_len(&mut self) -> u32 {
        let mut c: u32 = self.read_str(1).await.as_bytes()[0] as u32;
        if c & 0x80 == 0x00 {
        } else if c & 0xC0 == 0x80 {
            c &= !0xC0;
            c <<= 8;
            c += self.read_str(1).await.as_bytes()[0] as u32;
        } else if c & 0xE0 == 0xC0 {
            c &= !0xE0;
            c <<= 8;
            c += self.read_str(1).await.as_bytes()[0] as u32;
            c <<= 8;
            c += self.read_str(1).await.as_bytes()[0] as u32;
        } else if c & 0xF0 == 0xE0 {
            c &= !0xF0;
            c <<= 8;
            c += self.read_str(1).await.as_bytes()[0] as u32;
            c <<= 8;
            c += self.read_str(1).await.as_bytes()[0] as u32;
            c <<= 8;
            c += self.read_str(1).await.as_bytes()[0] as u32;
        } else if c & 0xF8 == 0xF0 {
            c = self.read_str(1).await.as_bytes()[0] as u32;
            c <<= 8;
            c += self.read_str(1).await.as_bytes()[0] as u32;
            c <<= 8;
            c += self.read_str(1).await.as_bytes()[0] as u32;
            c += self.read_str(1).await.as_bytes()[0] as u32;
            c <<= 8;
        }
        c
    }

    async fn read_word(&mut self) -> String {
        let len = self.read_len().await;
        let ret = self.read_str(len as usize).await;
        ret
    }

    async fn write_word(&mut self, w: String) {
        self.write_len(w.len() as u32).await;
        self.write_str(&w.as_bytes()).await;
    }

    pub async fn write_sentence(&mut self, words: Vec<String>) -> u32 {
        let mut ret: u32 = 0;
        for w in words {
            self.write_word(w).await;
            ret += 1;
        }
        self.write_word("".to_string()).await;
        ret
    }

    pub async fn read_sentence(&mut self) -> Vec<String> {
        let mut r: Vec<String> = Vec::new();
        loop {
            let w = self.read_word().await;
            if w[..].is_empty() {
                return r;
            }
            r.push(w);
        }
    }

    pub async fn talk(&mut self, words: Vec<String>) -> Vec<(String, BTreeMap<String, String>)> {
        if self.write_sentence(words).await == 0 {
            return vec![];
        }

        let mut r: Vec<(String, BTreeMap<String, String>)> = Vec::new();

        loop {
            let i: Vec<String> = self.read_sentence().await;
            if i.is_empty() {
                continue;
            }

            let reply: String = i[0].clone();
            let mut attrs: BTreeMap<String, String> = BTreeMap::new();

            for w in &i[1..] {
                match w[1..].find('=') {
                    Some(n) => {
                        attrs.insert(w[..n + 1].to_string(), w[(n + 2)..].to_string());
                    },

                    None => {
                        attrs.insert(w.clone(), "".to_string());
                    },
                };
            }
            r.push((reply.clone(), attrs));
            if reply == "!done" {
                return r;
            }
        }
    }

    pub async fn login(&mut self, username: &str, pwd: &str) -> Result<(), ApiError> {
        let res = self
            .talk(vec![
                r"/login".to_string(),
                format!("=name={}", username),
                format!("=password={}", pwd),
            ])
            .await;

        let (attribute, value) = res.first().unwrap();
        return match &attribute[..] {
            "!trap" => Err(ApiError::from_trap(value)),
            "!done" => Ok(()),
            _ => {
                Err(ApiError::Unknown(Some(format!(
                    "Login failed with unknown error {:?}",
                    res
                ))))
            },
        };
    }
}


use std::{convert::TryFrom, net::IpAddr};

use serde::de::{Error, Unexpected};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ApiError {
    #[error("API Returned Error")]
    Trap {
        message: String,
        category: Option<TrapCategory>,
    },
    #[error("Unknown error")]
    Unknown(Option<String>),
}

impl ApiError {
    pub fn from_trap(trap_content: &BTreeMap<String, String>) -> Self {
        let message = match trap_content.get("=message") {
            None => {
                return ApiError::Unknown(Some(format!(
                    "Could not parse trap values: {:?}",
                    trap_content
                )))
            },
            Some(i) => i.to_owned(),
        };

        ApiError::Trap {
            message,
            category: TrapCategory::try_from(trap_content.get("=category")).ok(),
        }
    }
}

#[derive(Error, Debug)]
pub enum TrapCategory {
    #[error("missing item or command")]
    C0,
    #[error("argument value failure")]
    C1,
    #[error("execution of command interrupted")]
    C2,
    #[error("scripting related failure")]
    C3,
    #[error("general failure")]
    C4,
    #[error("API related failure")]
    C5,
    #[error("TTY related failure")]
    C6,
    #[error("value generated with :return command")]
    C7,
}

impl TryFrom<Option<&String>> for TrapCategory {
    type Error = ();

    fn try_from(input: Option<&String>) -> Result<Self, Self::Error> {
        match input.ok_or(())?.parse::<u8>().map_err(|_| ())? {
            0 => Ok(Self::C0),
            1 => Ok(Self::C1),
            2 => Ok(Self::C2),
            3 => Ok(Self::C3),
            4 => Ok(Self::C4),
            5 => Ok(Self::C5),
            6 => Ok(Self::C6),
            7 => Ok(Self::C7),
            _ => Err(()),
        }
    }
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "kebab-case")]
pub struct DhcpLease {
    pub id: Option<String>,
    pub address: IpAddr,
    pub address_lists: String,
    #[serde(deserialize_with = "bool_from_string")]
    pub blocked: bool,
    pub dhcp_option: String,
    #[serde(deserialize_with = "bool_from_string")]
    pub disabled: bool,
    #[serde(deserialize_with = "bool_from_string")]
    pub dynamic: bool,
    #[serde(default)]
    pub host_name: String,
    pub last_seen: String,
    #[serde(default)]
    pub mac_address: eui48::MacAddress,
    pub radius: String,

    #[serde(default = "string_all")]
    pub server: String,
    pub status: String,

    #[serde(default, deserialize_with = "non_empty_str")]
    pub comment: Option<String>,

    #[serde(default, deserialize_with = "non_empty_str")]
    pub active_address: Option<String>,
    #[serde(default, deserialize_with = "non_empty_str")]
    pub active_mac_address: Option<String>,
    #[serde(default, deserialize_with = "non_empty_str")]
    pub active_server: Option<String>,
    #[serde(default, deserialize_with = "non_empty_str")]
    pub expires_after: Option<String>,
}

fn string_all() -> String {
    return String::from("all");
}

fn bool_from_string<'de, D>(input: D) -> Result<bool, D::Error>
where
    D: Deserializer<'de>,
{
    let result = String::deserialize(input)?;
    match &result[..] {
        "true" => Ok(true),
        "false" => Ok(false),
        _ => {
            Err(D::Error::invalid_value(
                Unexpected::Str(&result),
                &"Not a string boolean",
            ))
        },
    }
}

fn non_empty_str<'de, D: Deserializer<'de>>(d: D) -> Result<Option<String>, D::Error> {
    let o: String = String::deserialize(d)?;
    if o.is_empty() {
        Ok(None)
    } else {
        Ok(Some(o))
    }
}
