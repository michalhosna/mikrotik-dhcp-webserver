use std::{
    io,
    net::{SocketAddr, TcpListener, ToSocketAddrs},
    time::Duration,
};

use actix_web::{
    http::header::{CacheControl, CacheDirective},
    web,
    Error as ActixErr,
    HttpResponse,
    HttpServer,
};
use anyhow::{Context, Result};
use async_std::net::TcpStream;
use router_os::ApiRos;
use serde::{de::value::MapDeserializer, Deserialize};

use crate::{
    oui::{MacDescription, OuiDb},
    router_os::DhcpLease,
};

mod oui;
mod router_os;

#[actix_web::get("/")]
async fn index(
    app_config: web::Data<AppConfig>,
    oui: web::Data<OuiDb>,
) -> Result<HttpResponse, ActixErr> {
    let mut router_stream = match async_std::future::timeout(
        Duration::from_secs(3),
        TcpStream::connect(app_config.router_addr),
    )
    .await
    {
        Err(_) => panic!("Connection timed out"),
        Ok(stream) => {
            match stream {
                Ok(stream) => stream,
                Err(..) => panic!("Connection failed"),
            }
        },
    };

    let mut apiros = ApiRos::new(&mut router_stream);
    apiros
        .login(&app_config.login, &app_config.password)
        .await
        .unwrap();
    let res = apiros
        .talk(vec!["/ip/dhcp-server/lease/print".into()])
        .await;


    let mut leases: Vec<DhcpLease> = res
        .into_iter()
        .map(|i| i.1)
        .filter(|i| !i.is_empty())
        .map(|j| {
            DhcpLease::deserialize(MapDeserializer::<_, serde::de::value::Error>::new(
                j.into_iter().map(|(i, j)| (i[1..].to_owned(), j)),
            ))
            .unwrap()
        })
        .collect();

    leases.sort_by_key(|i| i.address);


    let tbody: String = leases
        .iter()
        .map(|lease| {
            let (vendor, vendor_tooltip): (String, String) =
                match oui.mac_description(lease.mac_address) {
                    MacDescription::Laa => {
                        (
                            "<small>LAA</small>".into(),
                            "Locally Administered Addresses".into(),
                        )
                    },
                    MacDescription::Broadcast => ("Broadcast".into(), String::default()),
                    MacDescription::Multicast => ("Multicast".into(), String::default()),
                    MacDescription::Oui(oui) => oui.short_long_desc(),
                    MacDescription::Unknown => ("🤷".into(), "Unknown".into()),
                };

            format!(
                // language=html
                r#"
        <tr class="entry">
            <td>{ip_address}</td>
            <td>{mac_address}</td>
            <td>{host_name}</td>
            <td>{comment}</td>
            <td>{lease_type}</td>
            <td>{active_ip_addr}</td>
            <td>{lease_time}</td>
            <td>{last_seen}</td>
            <td title="{vendor_tooltip}">{vendor}</td>
        <tr>
    "#,
                ip_address = lease.address,
                mac_address = lease.mac_address.to_hex_string().to_uppercase(),
                host_name = lease.host_name,
                comment = lease.comment.as_ref().unwrap_or(&String::from("")),
                lease_type = if lease.dynamic { "dynamic" } else { "static" },
                active_ip_addr = lease.active_address.as_ref().unwrap_or(&String::from("--")),
                lease_time = lease.expires_after.as_ref().unwrap_or(&String::from("--")),
                last_seen = if lease.last_seen == "never" {
                    "never".into()
                } else {
                    lease
                        .last_seen
                        .split_inclusive(char::is_alphabetic)
                        .take(2)
                        .collect::<Vec<_>>()
                        .join("")
                },
                vendor_tooltip = vendor_tooltip,
                vendor = vendor
            )
        })
        .fold(String::new(), |a, b| a + &b + "\n");


    let body = std::str::from_utf8(include_bytes!(concat!(
        env!("CARGO_MANIFEST_DIR"),
        "/html/index.html"
    )))
    .unwrap()
    .replace("{TBODY}", &tbody);

    Ok(HttpResponse::Ok()
        .insert_header(CacheControl(vec![
            CacheDirective::NoCache,
            CacheDirective::Private,
            CacheDirective::MaxAge(0),
        ]))
        .content_type("text/html")
        .body(body))
}

#[actix_web::get("/app.js")]
async fn app_js() -> Result<HttpResponse, ActixErr> {
    Ok(HttpResponse::Ok()
        .insert_header(CacheControl(vec![
            CacheDirective::Private,
            CacheDirective::MaxAge(60),
        ]))
        .content_type("text/javascript")
        .body(&include_bytes!(concat!(env!("CARGO_MANIFEST_DIR"), "/html/app.js"))[..]))
}

#[actix_web::get("/style.css")]
async fn style_css() -> Result<HttpResponse, ActixErr> {
    Ok(HttpResponse::Ok()
        .insert_header(CacheControl(vec![
            CacheDirective::Private,
            CacheDirective::MaxAge(60),
        ]))
        .content_type("text/css")
        .body(&include_bytes!(concat!(env!("CARGO_MANIFEST_DIR"), "/html/style.css"))[..]))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let app_config = cli_args().map_err(|e| io::Error::new(io::ErrorKind::InvalidInput, e))?;

    let listener = TcpListener::bind(&app_config.bind_addr)?;
    println!("Starting server at {}", listener.local_addr()?);

    let oui = web::Data::new(
        OuiDb::new(
            std::str::from_utf8(include_bytes!(concat!(
                env!("CARGO_MANIFEST_DIR"),
                "/manuf.txt"
            )))
            .unwrap()
            .lines(),
        )
        .unwrap(),
    );

    HttpServer::new(move || {
        actix_web::App::new()
            .data(app_config.clone())
            .app_data(oui.clone())
            .service(index)
            .service(style_css)
            .service(app_js)
    })
    .listen(listener)?
    .run()
    .await
}

fn cli_args() -> Result<AppConfig> {
    use clap::{App, Arg};

    let matches = App::new(env!("CARGO_PKG_NAME"))
        .version(concat!(
            env!("CARGO_PKG_VERSION"),
            " git:",
            env!("VERGEN_GIT_SHA")
        ))
        .long_version(concat!(
            env!("CARGO_PKG_VERSION"),
            "\n",
            "\nCommit SHA:\t",
            env!("VERGEN_GIT_SHA"),
            "\nCommit Date:\t",
            env!("VERGEN_GIT_COMMIT_TIMESTAMP"),
            "\nrustc Version:\t",
            env!("VERGEN_RUSTC_SEMVER"),
            "\nrustc SHA:\t",
            env!("VERGEN_RUSTC_COMMIT_HASH"),
            "\ncargo Target:\t",
            env!("VERGEN_CARGO_TARGET_TRIPLE"),
            "\ncargo Profile:\t",
            env!("VERGEN_CARGO_PROFILE"),
        ))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(concat!(
            env!("CARGO_PKG_DESCRIPTION"),
            "\n\n",
            env!("CARGO_PKG_REPOSITORY")
        ))
        .arg(
            Arg::with_name("bind-addr")
                .long("bind-addr")
                .takes_value(true)
                .help("Socket address to bind to, e.g. 0.0.0.0:8080")
                .required(true),
        )
        .arg(
            Arg::with_name("router-addr")
                .long("router-addr")
                .takes_value(true)
                .help("Socket address of router api, e.g. 192.168.88.1:8728")
                .required(true),
        )
        .arg(
            Arg::with_name("login")
                .long("login")
                .takes_value(true)
                .help("RouterOS Login, e.g. admin")
                .required(true),
        )
        .arg(
            Arg::with_name("password")
                .long("password")
                .takes_value(true)
                .help("RouterOS Password, value from env ROUTEROS_PASSWD used by default"),
        )
        .get_matches();

    let bind_addr = matches
        .value_of("bind-addr")
        .expect("Could not get value of bind-addr")
        .to_socket_addrs()
        .with_context(|| "Failed to parse bind-addr")?
        .next()
        .with_context(|| "Failed to obtain bind-addr")?;

    let router_addr = matches
        .value_of("router-addr")
        .expect("Could not get value of router-addr")
        .to_socket_addrs()
        .with_context(|| "Failed to parse router-addr")?
        .next()
        .with_context(|| "Failed to obtain router-addr")?;


    let login = matches
        .value_of("login")
        .expect("Could not get value of login");

    let password = matches
        .value_of("password")
        .map(Into::into)
        .unwrap_or_else(|| {
            std::env::var("ROUTEROS_PASSWD")
                .expect("Could not get value of env var ROUTEROS_PASSWD")
        });

    Ok(AppConfig {
        bind_addr,
        router_addr,
        login: login.to_string(),
        password,
    })
}

#[derive(Clone, Debug)]
struct AppConfig {
    pub bind_addr: SocketAddr,
    pub router_addr: SocketAddr,
    pub login: String,
    pub password: String,
}
